# FabZero @ FabLab ULB - Linux Command line

## Goal of this unit

The goal of this unit is to learn to use to use a few fundamental UNIX commands to get started using the terminal.


## Command lines

Most of us have learned to use programs through a **Graphical User Interface GUI**. However, it can be very powerful to learn to use programs through a **a Command Line user Interface CLI** (terminal) an to use UNIX commands.

Why is it working so well ?

* [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy)

A useful Unix shell (command-line interpreter) :

* [BASH (Unix Shell)](https://en.wikipedia.org/wiki/Bash_(Unix_shell)), native on Linux and Mac Os. [Install it on windows.](https://korben.info/installer-shell-bash-linux-windows-10.html)

Advantages :

* use linux program that have no GUI
* faster, lighter to execute program
* use shell script

Some tutorials :

* [FabZero tutorial](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md)
* [Ubuntu tutorials - The Linux command line for beginner](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview)
* [openclassrooms - Initiez-vous à Linux (french)](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/42867-introduction-aux-scripts-shell)

> **Learn by doing** : Learn to use [a few fundamental UNIX commands ](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html) to get started using a command-line interpreter (navigate the filesystem, opening files, running programs) - ls, cd, mkdir, cp, mv, rm, man


## Credits
This unit is build-on the [Official FabZero Program](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md) and adapted by Denis Terwagne for the *FabZero @ FabLab ULB program*.
