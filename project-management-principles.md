# FabZero @ FabLab ULB - Project management principles

## Goal of this unit

The goal of this unit is to learn some tips for successful Project Management

## Project Management Techniques

* [As-You-Work Documentation](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#as-you-work-documentation)
* [Modular & Hierarchical Planning](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#modular-hierarchical-planning)
* Demand- vs [Supply-Side Time Management](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#supply-side-time-management)
* [Spiral Development](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#spiral-development)
* [Triage](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#triage)

> **Learn by doing** : test and apply those principles all along the class

## Some more :

- Parkinson Law
  
> “Work expands so as to fill the time available for its completion.”

- Hofstadter’s Law

> “it always takes longer than expected, even taking into account Hofstadter’s Law.”

## Ressources

* [Effrayé par l'ampleur d'un projet à accomplir ? lisez cet article de blog. (french)](https://dtwg4.github.io/blog-flexible-jekyll/techniques-gestion-projet/)

## Credits
This unit is build-on the [Fab Academy Program](https://fabacademy.org/) and adapted by Denis Terwagne for the *FabZero @ FabLab ULB program*.


